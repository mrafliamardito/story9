from django.urls import path
from . import views

app_name='jadwal'
urlpatterns = [
    path('', views.Landing,name='Landing'),
    path('signup', views.signup,name='signup'),
    path('main', views.main,name='main'),
    path('logout',views.logout,name='logout')

]


